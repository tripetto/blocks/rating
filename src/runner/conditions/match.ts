/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    alias: "rating",
})
export class RatingMatchCondition extends ConditionBlock<{
    readonly stars: number;
}> {
    @condition
    stars(): boolean {
        const ratingSlot = this.valueOf<number>("rating");

        return (ratingSlot && ratingSlot.value === this.props.stars) || false;
    }
}
