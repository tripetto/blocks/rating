/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "@tripetto/builder";
import { TMode } from "../../runner/conditions/mode";
import { Rating } from "..";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:compare`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:rating", "Compare rating");
    },
})
export class RatingCompareCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    get rating() {
        return (
            (this.node?.block instanceof Rating && this.node.block) || undefined
        );
    }

    get icon() {
        return this.rating?.shapeIcon || ICON;
    }

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Number) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:scale",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "defined":
                    return `@${slot.id} ${pgettext("block:rating", "rated")}`;
                case "undefined":
                    return `@${slot.id} ${pgettext(
                        "block:rating",
                        "not rated"
                    )}`;
                case "not-equal":
                    return `@${slot.id} \u2260 ${value}`;
                case "above":
                case "below":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    private parse(
        slot: Slots.Number,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.toString(value);
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return "@" + value;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:rating", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is lower than"
                            ),
                            value: "below",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is higher than"
                            ),
                            value: "above",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is not between"
                            ),
                            value: "not-between",
                        },
                        {
                            label: pgettext("block:rating", "Rating is given"),
                            value: "defined",
                        },
                        {
                            label: pgettext(
                                "block:rating",
                                "Rating is not given"
                            ),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    from.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    to.visible(
                        mode.value === "between" || mode.value === "not-between"
                    );

                    switch (mode.value) {
                        case "equal":
                            from.title = pgettext(
                                "block:rating",
                                "If rating equals"
                            );
                            break;
                        case "not-equal":
                            from.title = pgettext(
                                "block:rating",
                                "If rating not equals"
                            );
                            break;
                        case "below":
                            from.title = pgettext(
                                "block:rating",
                                "If rating is lower than"
                            );
                            break;
                        case "above":
                            from.title = pgettext(
                                "block:rating",
                                "If rating is higher than"
                            );
                            break;
                        case "between":
                            from.title = pgettext(
                                "block:rating",
                                "If rating is between"
                            );
                            break;
                        case "not-between":
                            from.title = pgettext(
                                "block:rating",
                                "If rating is not between"
                            );
                            break;
                    }
                }),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:rating", "Use fixed number"))
                .min(0)
                .max(this.rating?.steps || 5)
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isString(value) ? value : undefined,
                false,
                this.slot?.id
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:rating", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"number" | "variable">(
                            [
                                {
                                    label: pgettext("block:rating", "Number"),
                                    value: "number",
                                },
                                {
                                    label: pgettext("block:rating", "Value"),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value) ? "variable" : "number"
                        ).on((type) => {
                            numberControl.visible(type.value === "number");
                            variableControl.visible(type.value === "variable");

                            if (numberControl.isObservable) {
                                numberControl.focus();
                            }
                        }),
                        numberControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            pgettext("block:rating", "If rating equals"),
            this.mode !== "defined" && this.mode !== "undefined"
        );
        const to = addCondition(
            "to",
            pgettext("block:rating", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
