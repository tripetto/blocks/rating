/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { Rating } from "..";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    alias: "rating",
    get label() {
        return pgettext("block:rating", "Match rating");
    },
})
export class RatingMatchCondition extends ConditionBlock {
    @definition
    @affects("#name")
    stars = 0;

    get rating() {
        return (
            (this.node?.block instanceof Rating && this.node.block) || undefined
        );
    }

    get icon() {
        return this.rating?.shapeIcon || ICON;
    }

    get name() {
        return Rating.getShapeLabel(this.rating?.shape || "stars", this.stars);
    }

    get title() {
        return this.node?.label;
    }
}
